package com.appemble.avm.bootstrap;

import java.lang.reflect.Field;

import org.xmlpull.v1.XmlPullParser;

import com.appemble.avm.Cache;
import com.appemble.avm.ConfigManager;
import com.appemble.avm.LogManager;
import com.appemble.coverflow.sample.R;

public class Bootstrap extends com.appemble.avm.Bootstrap {
	public Bootstrap() {}
	
	public boolean execute() {
		// Here read all variables of R.xml
		// Parse all config files and add it to _configManager of the appemble
		// library.
		Field fields[] = R.xml.class.getDeclaredFields();
		try {
			for (int i = 0; i < fields.length; i++) {
				if (fields[i].getName().contains("config")
						&& fields[i].getType() == int.class) {
					fields[i].setAccessible(true);
					XmlPullParser xpp = Cache.context.getResources().getXml(
							((Integer) fields[i].get(R.xml.class)).intValue());
					ConfigManager.getInstance().readConfigFile(xpp);
				}
			}
			ConfigManager.getInstance().updateMethodsForControls();
		} catch (IllegalArgumentException e) {
			System.out.println("Unable to read configuration files. Probable causes \r\n Configuration file in appemble-android-library/res/xml/appemble_android_library_config.xml is corrupted.");
			return false;
		} catch (IllegalAccessException e) {
			System.out.println("Unable to read configuration files. Probable causes \r\n Configuration file in appemble-android-library/res/xml/appemble_android_library_config.xml is corrupted.");
			return false;
		}
		return true;
	}

	public String getValue(String sKey) {
		if (null == sKey)
			return null;
		Field fields[] = R.string.class.getDeclaredFields();
		try {
			for (int i = 0; i < fields.length; i++) {
				if (fields[i].getName().equals(sKey)) {
					fields[i].setAccessible(true);
					return Cache.context.getResources().getString(
							((Integer) fields[i].get(R.string.class))
									.intValue());
				}
			}
		} catch (IllegalArgumentException e) {
			LogManager.logWTF(e, "Unable to read configuration files.");
		} catch (IllegalAccessException e) {
			LogManager.logWTF(e, "Unable to read configuration files.");
		}
		return null;
	}

	public int getIntValue(String sKey) {
		if (null == sKey)
			return -1;
		Field fields[] = R.integer.class.getDeclaredFields();
		try {
			for (int i = 0; i < fields.length; i++) {
				if (fields[i].getName().equals(sKey)) {
					fields[i].setAccessible(true);
					return Cache.context.getResources().getInteger(
							((Integer) fields[i].get(R.integer.class))
									.intValue());
				}
			}
		} catch (IllegalArgumentException e) {
			LogManager.logWTF(e, "Unable to read configuration files.");
		} catch (IllegalAccessException e) {
			LogManager.logWTF(e, "Unable to read configuration files.");
		}
		return -1;
	}

	public String getSValue(String sKey) {
		return null;
	}
//  Uncomment if raw is defined and a URI is needed for the raw file.]	
//	public Uri getRawUri(String sName) {
//		Field fields[] = R.raw.class.getDeclaredFields();
//		try {
//			for (int i = 0; i < fields.length; i++) {
//				if (fields[i].getName().equals(sName) && fields[i].getType() == int.class) {
//					fields[i].setAccessible(true);
//					Uri video = Uri.parse("android.resource://" + Cache.context.getPackageName() + "/"
//				    + ((Integer) fields[i].get(R.string.class))
//					.intValue());			
//					return video;
//				}
//			}
//		} catch (IllegalArgumentException e) {
//			System.out.println("Unable to read configuration files. Probable causes \r\n Configuration file in appemble-android-library/res/xml/appemble_android_library_config.xml is corrupted.");
//		} catch (IllegalAccessException e) {
//			LogManager.logWTF(e, "Unable to read configuration files.");
//		}
//		return null;
//	}

//  uncomment if encrypted database is defined	
//	public String getContentDbEncryptionKeys() {
//		return "";
//	}
//	
//	uncomment if SQL script is present to do version updates on content database.
//    public void onSystemDbUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//    	return;
//    }
//
//    public void onContentDbUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
//    	return;
//    }
}
